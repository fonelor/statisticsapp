package com.rolsoft.statisticsapp

import com.rolsoft.statisticsapp.statistics.StatisticsStorage
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import java.util.concurrent.TimeUnit

@Configuration
class StorageConfig {
    @Bean
    @Scope
    fun statisticsStorage(@Value("\${statistics.aggregateSize}") statSize: Long): StatisticsStorage {
        return StatisticsStorage(TimeUnit.SECONDS.toMillis(statSize).toInt())
    }
}