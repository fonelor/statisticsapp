package com.rolsoft.statisticsapp

import java.time.Instant

interface TimeProvider {
    fun now(): Instant
}