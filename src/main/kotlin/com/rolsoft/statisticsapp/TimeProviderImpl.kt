package com.rolsoft.statisticsapp

import org.springframework.stereotype.Service
import java.time.Instant

@Service
class TimeProviderImpl : TimeProvider {
    override fun now(): Instant {
        return Instant.now()
    }
}