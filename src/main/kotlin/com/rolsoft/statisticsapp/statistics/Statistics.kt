package com.rolsoft.statisticsapp.statistics

import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Statistics data
 * Not thread safe
 */
data class Statistics(var sum: BigDecimal = BigDecimal.ZERO,
                      var avg: BigDecimal = BigDecimal.ZERO,
                      var count: Long = 0L) {
    lateinit var max: BigDecimal
    lateinit var min: BigDecimal

    constructor(value: BigDecimal) : this(value, value, 1L) {
        max = value
        min = value
    }

    fun merge(statistics: Statistics) {
        // merge only if statistics has some value
        if (statistics.count != 0L) {
            if (count == 0L) {
                avg = statistics.avg
                max = statistics.max
                min = statistics.min
            } else {
                avg = (avg + statistics.avg).divide(BigDecimal(2), RoundingMode.FLOOR)
                max = maxOf(max, statistics.max)
                min = minOf(min, statistics.min)
            }
            sum += statistics.sum
            count += statistics.count
        }
    }
}