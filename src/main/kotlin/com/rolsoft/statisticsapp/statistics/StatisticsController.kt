package com.rolsoft.statisticsapp.statistics

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/statistics",
        produces = [MediaType.APPLICATION_JSON_VALUE])
class StatisticsController(private val statisticsService: StatisticsServiceImpl) {
    private val log = LoggerFactory.getLogger(StatisticsController::class.java)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getStatistics(): Statistics {
        val statistics = statisticsService.calculateStatistics()
        log.debug("Statistics for last 60 seconds is {}", statistics)
        return statistics
    }
}