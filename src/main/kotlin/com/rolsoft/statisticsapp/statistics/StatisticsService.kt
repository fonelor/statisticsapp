package com.rolsoft.statisticsapp.statistics

interface StatisticsService {
    fun calculateStatistics(): Statistics
}