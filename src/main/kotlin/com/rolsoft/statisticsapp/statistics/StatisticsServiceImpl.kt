package com.rolsoft.statisticsapp.statistics

import com.rolsoft.statisticsapp.TimeProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class StatisticsServiceImpl(private val statisticsStorage: StatisticsStorage,
                            private val timeProvider: TimeProvider,
                            @Value("\${statistics.aggregateSize}") private val statSize: Long) : StatisticsService {
    override fun calculateStatistics(): Statistics {
        val now = timeProvider.now()
        return statisticsStorage.calculateStatistics(now.minusSeconds(statSize), now)
    }
}