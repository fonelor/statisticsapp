package com.rolsoft.statisticsapp.statistics

import java.math.BigDecimal
import java.time.Instant

private val EMPTY: Statistics = Statistics()

/**
 * Size of storage ring buffer in milliseconds
 */
class StatisticsStorage(size: Int) {
    private val buckets = Array(size) { StatBucket() }

    fun add(value: BigDecimal, ts: Instant) {
        val millis = ts.toEpochMilli()
        val bucketNum = millis % buckets.size
        buckets[bucketNum.toInt()].update(Statistics(value), millis)
    }

    fun calculateStatistics(fromI: Instant, toI: Instant): Statistics {
        val from = fromI.toEpochMilli()
        val to = toI.toEpochMilli()

        val statistics = Statistics()
        for (bucket in buckets) {
            val bucketValue = bucket.getValue(from, to)
            if (bucketValue == EMPTY) {
                continue
            }
            statistics.merge(bucketValue)
        }
        return statistics
    }

    private class StatBucket {
        private var statistics = Statistics()
        private var timestamp = 0L

        /**
         * Updates bucket value
         *
         * * if [ts] is the same as stored in bucket then statistics shall be merged<br/>
         * * if [ts] is greater than stored in bucket, statistics shall be replaced
         * * if [ts] is less than stored in bucket - nothing shall be done
         */
        fun update(value: Statistics, ts: Long) = synchronized(this) {
            if (ts == timestamp) { // same milli
                statistics.merge(value)
            } else if (ts > timestamp) { // new cycle
                timestamp = ts
                statistics = value
            }
            // in the past - ignore
        }

        /**
         * Returns current value if timestamp is in range of from and to and zero otherwise
         */
        fun getValue(from: Long, to: Long): Statistics = synchronized(this) {
            if (timestamp in from..to) {
                statistics
            } else EMPTY
        }
    }
}