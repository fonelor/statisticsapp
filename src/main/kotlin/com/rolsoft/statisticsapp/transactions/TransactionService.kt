package com.rolsoft.statisticsapp.transactions

interface TransactionService {
    fun register(transaction: Transaction): TransactionRegistrationStatus

    enum class TransactionRegistrationStatus {
        REGISTERED,
        TOO_OLD,
        IN_THE_FUTURE
    }
}