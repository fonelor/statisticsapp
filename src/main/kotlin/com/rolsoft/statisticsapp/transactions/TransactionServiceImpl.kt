package com.rolsoft.statisticsapp.transactions

import com.rolsoft.statisticsapp.TimeProvider
import com.rolsoft.statisticsapp.statistics.StatisticsStorage
import com.rolsoft.statisticsapp.transactions.TransactionService.TransactionRegistrationStatus
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.Duration

@Service
class TransactionServiceImpl(private val timeProvider: TimeProvider,
                             private val statisticsStorage: StatisticsStorage,
                             @Value("\${statistics.aggregateSize}") private val statSize: Long) : TransactionService {
    override fun register(transaction: Transaction): TransactionRegistrationStatus {
        val now = timeProvider.now()
        return when {
            transaction.timestamp.isAfter(now) -> {
                TransactionRegistrationStatus.IN_THE_FUTURE
            }
            Duration.ofSeconds(statSize).minus(Duration.between(transaction.timestamp, now)).isNegative -> {
                TransactionRegistrationStatus.TOO_OLD
            }
            else -> {
                statisticsStorage.add(transaction.amount, transaction.timestamp)
                TransactionRegistrationStatus.REGISTERED
            }
        }
    }

}
