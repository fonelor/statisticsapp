package com.rolsoft.statisticsapp.transactions

import com.rolsoft.statisticsapp.transactions.TransactionService.TransactionRegistrationStatus.*
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/transactions",
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE])
class TransactionsController(private val transactionService: TransactionService) {
    private val log = LoggerFactory.getLogger(TransactionsController::class.java)

    @PostMapping
    fun createTransaction(@RequestBody transaction: Transaction): ResponseEntity<Unit> {
        val result = transactionService.register(transaction)
        log.debug("Received transaction: {}, result is {}", transaction, result)
        return when (result) {
            REGISTERED -> {
                ResponseEntity.status(HttpStatus.CREATED).build()
            }
            TOO_OLD -> {
                ResponseEntity.noContent().build()
            }
            IN_THE_FUTURE -> {
                ResponseEntity.unprocessableEntity().build()
            }
        }
    }

}