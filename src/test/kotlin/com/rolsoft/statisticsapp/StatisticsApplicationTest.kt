package com.rolsoft.statisticsapp

import com.rolsoft.statisticsapp.statistics.Statistics
import com.rolsoft.statisticsapp.transactions.Transaction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.http.HttpStatus
import java.math.BigDecimal
import java.time.Instant

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class StatisticsApplicationTest(@Autowired private val restTemplate: TestRestTemplate) {

    @Test
    internal fun `Insert 1000 transactions in one bucket`() {
        val timestamp = Instant.now()
        val amount = BigDecimal.ONE

        val transaction = Transaction(amount, timestamp)
        for (i in 1..1000) {
            val txResponse = restTemplate.postForEntity<Unit>("/transactions", transaction)
            assertEquals(HttpStatus.CREATED, txResponse.statusCode)
        }

        val statisticsResponse = restTemplate.getForEntity<Statistics>("/statistics")

        assertEquals(HttpStatus.OK, statisticsResponse.statusCode)
        val statistics = statisticsResponse.body!!
        assertEquals(1000, statistics.count)
        assertEquals(BigDecimal.valueOf(1000), statistics.sum)
        assertEquals(BigDecimal.ONE, statistics.avg)
        assertEquals(BigDecimal.ONE, statistics.max)
        assertEquals(BigDecimal.ONE, statistics.min)
    }
}