package com.rolsoft.statisticsapp

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.time.Instant

internal class TimeProviderImplTest {
    private val timeProvider = TimeProviderImpl()

    @Test
    internal fun `Assert that actual time is returned`() {
        val before = Instant.now()
        Thread.sleep(1L)
        val now = timeProvider.now()
        Thread.sleep(1L)
        val after = Instant.now()

        assertTrue(now.isAfter(before))
        assertTrue(now.isBefore(after))
    }
}