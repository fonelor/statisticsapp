package com.rolsoft.statisticsapp.statistics

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpStatus
import java.math.BigDecimal

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [StatisticsControllerTest.Configuration::class])
internal class StatisticsControllerTest(@Autowired val restTemplate: TestRestTemplate,
                                        @Autowired val statisticsService: StatisticsServiceImpl) {

    @Test
    internal fun `Assert that 200 ok return with the result`() {
        every { statisticsService.calculateStatistics() } returns Statistics(BigDecimal.ZERO)

        val entity = restTemplate.getForEntity<Statistics>("/statistics")
        assertEquals(HttpStatus.OK, entity.statusCode)
    }

    @TestConfiguration
    class Configuration {
        @Bean
        fun statisticsService(): StatisticsServiceImpl = mockk()
    }

}