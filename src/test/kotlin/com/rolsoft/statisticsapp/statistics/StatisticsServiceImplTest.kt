package com.rolsoft.statisticsapp.statistics

import com.rolsoft.statisticsapp.TimeProvider
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.Instant

internal class StatisticsServiceImplTest {
    private val statisticsStorage = mockk<StatisticsStorage>()
    private val timeProvider = mockk<TimeProvider>()
    private val statSize = 60L

    private val statisticsService = StatisticsServiceImpl(statisticsStorage, timeProvider, statSize)

    @Test
    internal fun `Assert that statistics queried according stat size`() {
        val now = Instant.now()
        val statistics = Statistics(BigDecimal.ZERO)

        every { timeProvider.now() } returns now
        every { statisticsStorage.calculateStatistics(any(), any()) } returns statistics

        val calculatedStatistics = statisticsService.calculateStatistics()

        assertEquals(statistics, calculatedStatistics)

        verify(exactly = 1) {
            statisticsStorage.calculateStatistics(now.minusMillis(statSize * 1000), now)
        }
    }
}