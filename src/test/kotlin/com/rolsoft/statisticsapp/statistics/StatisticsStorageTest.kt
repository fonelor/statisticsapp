package com.rolsoft.statisticsapp.statistics

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.Instant

internal class StatisticsStorageTest {
    private val statisticsStorage = StatisticsStorage(10)

    @Test
    internal fun `Assert that statistics for one transaction is calculated`() {
        val ts = Instant.now()
        statisticsStorage.add(BigDecimal.ONE, ts)
        val countStatistics = statisticsStorage.calculateStatistics(ts.minusSeconds(60L), ts)

        assertEquals(BigDecimal.ONE, countStatistics.max)
        assertEquals(BigDecimal.ONE, countStatistics.min)
        assertEquals(BigDecimal.ONE, countStatistics.sum)
        assertEquals(BigDecimal.ONE, countStatistics.avg)
        assertEquals(1L, countStatistics.count)
    }

    @Test
    internal fun `Assert that statistics for two transactions is calculated`() {
        val ts = Instant.now()
        statisticsStorage.add(BigDecimal.ONE, ts)
        statisticsStorage.add(BigDecimal.ONE, ts.minusMillis(5L))

        val countStatistics = statisticsStorage.calculateStatistics(ts.minusSeconds(60L), ts)
        assertEquals(BigDecimal.ONE, countStatistics.max)
        assertEquals(BigDecimal.ONE, countStatistics.min)
        assertEquals(BigDecimal(2.0), countStatistics.sum)
        assertEquals(BigDecimal.ONE, countStatistics.avg)
        assertEquals(2L, countStatistics.count)
    }

    @Test
    internal fun `Assert that statistics calculated only for transactions that are in time window`() {
        val ts = Instant.now()
        statisticsStorage.add(BigDecimal.ONE, ts)
        statisticsStorage.add(BigDecimal.ONE, ts.plusMillis(5L))

        val countStatistics = statisticsStorage.calculateStatistics(ts.minusSeconds(60L), ts)
        assertEquals(BigDecimal.ONE, countStatistics.max)
        assertEquals(BigDecimal.ONE, countStatistics.min)
        assertEquals(BigDecimal.ONE, countStatistics.sum)
        assertEquals(BigDecimal.ONE, countStatistics.avg)
        assertEquals(1L, countStatistics.count)
    }

    @Test
    internal fun `Assert that statistics for two transactions with the same timestamp is calculated`() {
        val ts = Instant.now()
        statisticsStorage.add(BigDecimal.ONE, ts)
        statisticsStorage.add(BigDecimal.ONE, ts)

        val countStatistics = statisticsStorage.calculateStatistics(ts.minusSeconds(60L), ts)
        assertEquals(BigDecimal.ONE, countStatistics.max)
        assertEquals(BigDecimal.ONE, countStatistics.min)
        assertEquals(BigDecimal(2.0), countStatistics.sum)
        assertEquals(BigDecimal.ONE, countStatistics.avg)
        assertEquals(2L, countStatistics.count)
    }
}