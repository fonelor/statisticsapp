package com.rolsoft.statisticsapp.transactions

import com.rolsoft.statisticsapp.TimeProvider
import com.rolsoft.statisticsapp.statistics.StatisticsStorage
import com.rolsoft.statisticsapp.transactions.TransactionService.TransactionRegistrationStatus.*
import io.mockk.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.Instant

internal class TransactionServiceImplTest {

    private val timeProvider = mockk<TimeProvider>()
    private val statisticsStorage = mockk<StatisticsStorage>()
    private val transactionService = TransactionServiceImpl(timeProvider, statisticsStorage, 60L)

    @Test
    internal fun `Assert that transaction is registered when it is in time range`() {
        val now = Instant.now()
        every { timeProvider.now() } returns now
        every { statisticsStorage.add(any(), any()) } just Runs

        val transaction = Transaction(BigDecimal.ONE, now)
        val status = transactionService.register(transaction)

        assertEquals(REGISTERED, status)
        verify(exactly = 1) { statisticsStorage.add(transaction.amount, transaction.timestamp) }
    }

    @Test
    internal fun `Assert that transaction is not registered when it is in the future`() {
        val now = Instant.now()
        every { timeProvider.now() } returns now

        val status = transactionService.register(Transaction(BigDecimal.ONE,
                now.plusMillis(1000L)))

        assertEquals(IN_THE_FUTURE, status)
    }

    @Test
    internal fun `Assert that transaction is not registered when it older than time range`() {
        val now = Instant.now()
        every { timeProvider.now() } returns now

        val status = transactionService.register(Transaction(BigDecimal.ONE,
                now.minusMillis(60 * 1000 + 1)))

        assertEquals(TOO_OLD, status)
    }
}