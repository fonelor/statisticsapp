package com.rolsoft.statisticsapp.transactions

import com.rolsoft.statisticsapp.transactions.TransactionService.TransactionRegistrationStatus.*
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.exchange
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.context.annotation.Bean
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import java.math.BigDecimal
import java.net.URI
import java.time.Instant

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class TransactionsControllerTest(@Autowired val restTemplate: TestRestTemplate,
                                          @Autowired val transactionService: TransactionService) {
    @Test
    internal fun `Assert transaction create returns 400 when json is invalid`() {
        val requestEntity = RequestEntity.post(URI("/transactions"))
                .contentType(MediaType.APPLICATION_JSON)
                .body(""" { "asdsadas} """)
        val entity = restTemplate.exchange<Unit>(requestEntity.url, HttpMethod.POST, requestEntity)
        assertEquals(HttpStatus.BAD_REQUEST, entity.statusCode)
    }

    @Test
    internal fun `Assert transaction create returns created status`() {
        every { transactionService.register(any()) } returns REGISTERED

        val entity = restTemplate.postForEntity<Unit>("/transactions",
                Transaction(BigDecimal(13.0), Instant.now()))
        assertEquals(HttpStatus.CREATED, entity.statusCode)
    }

    @Test
    internal fun `Assert 422 on transaction in future`() {
        every { transactionService.register(any()) } returns IN_THE_FUTURE

        val entity = restTemplate.postForEntity<Unit>("/transactions",
                Transaction(BigDecimal(13.0), Instant.now()))
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, entity.statusCode)
    }

    @Test
    internal fun `Assert no content on transaction in too old`() {
        every { transactionService.register(any()) } returns TOO_OLD

        val entity = restTemplate.postForEntity<Unit>("/transactions",
                Transaction(BigDecimal(13.0), Instant.now()))
        assertEquals(HttpStatus.NO_CONTENT, entity.statusCode)
    }

    @TestConfiguration
    class TransactionControllerConfig {
        @Bean
        fun transactionService() = mockk<TransactionService>()
    }
}